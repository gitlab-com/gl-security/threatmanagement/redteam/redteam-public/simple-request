This doesn't do much! It's a very simple static HTML site that uses JavaScript to contact your localhost address.

More details can be found in this [blog](https://about.gitlab.com/blog/2021/09/07/why-are-developers-vulnerable-to-driveby-attacks/) posting.
